Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "citations#index"
  get 'citations' => 'citations#index'
  get 'citations/new' => 'citations#new'
  post 'citations' => 'citations#create'
  # resources :citations
end
