require 'rails_helper'

feature "Filling citation form", type: :feature do

  background do
    visit '/citations/new'
  end

  given(:title) do
    'Self-setting kinetics of new type calcium phosphate bioactive bone cement: A thermokinetics study'
  end

  feature "Successful path" do 

    scenario "Redirects to citations list" do
      within("#new_citation") do
        fill_in 'Add author/authors of publication:', with: 'Zhou et.al'
        fill_in 'Add title of publication:', with: title
      end
      click_button "Add citation"
      expect(page).to have_content 'Citations'
    end

    scenario "Creates citation" do
      within("#new_citation") do
        fill_in 'Add author/authors of publication:', with: 'Zhou et.al'
        fill_in 'Add title of publication:', with: title
      end
      expect { click_button "Add citation" }.to change { Citation.count }.by(1) 

      citation = Citation.last
      expect(citation.author).to eq("Zhou et.al")
      expect(citation.title).to eq(title)  
    end

  end

  feature "Validation errors" do 

    scenario "Missing author" do
      within("#new_citation") do
        fill_in 'Add author/authors of publication:', with: ''
        fill_in 'Add title of publication:', with: title
      end
      click_button "Add citation"
      expect(page).to_not have_content 'Citations'
      expect(page).to have_content 'Please add author/authors of publication.'
    end
    
  end

end

