class CitationsController < ApplicationController
  def index
    @citation = Citation.all
  end

  def new
    @citation = Citation.new
  end

  def create
    @citation = Citation.create(create_params)

    if @citation.persisted?
      redirect_to "/citations"
    else
      render :new
    end
    
  end

  private

  def create_params
    params.require(:citation).permit(:author, :title, :year)
  end

end
