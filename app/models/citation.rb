class Citation < ApplicationRecord
  validates :author, presence: { message: "Please add author/authors of publication." }
  validates :title, presence: { message: "Please add title of publication." }
  validates :year, presence: true
end
