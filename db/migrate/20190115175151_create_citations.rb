class CreateCitations < ActiveRecord::Migration[5.2]
  def change
    create_table :citations do |t|

      t.timestamps
      t.string :author
      t.string :title
      t.integer :year      
    end
  end
end
